# Project Assessment Platform

This is aProject Assessment Platform demo project for practicing perpose.

## Key Technologies

**Server-Side:** Node JS, Express JS , TypeScript

**Database:** MongoDB (with ODM mongoose)

## Key Roles

- Admin
- Mentor
- Student

## Key Features

- Login System with email and password.
- User Authentication with JWT token
- Forgot password functionlity in three steps
- Existing password update functionlity in two steps
- Admin can create update delete a student
- Admin can see all student with pagination and searching functionlity
- Admin can register and delete a Mentor
- Admin and Mentor can upate a Mentor
- Admin can see all Mentor with pagination and searching functionlity
- Admin and Mentor can craete update and delete Assessment
- Admin and Mentor can see all Assessment by searching, sorting and filtering by many ways and get data in a pagination.
- Student can submit own assinged Assessment response via link or upload a file in a pdf format
- Mentor and Admin can grade and remark a particular assessment that is selected or rejected

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/SY-Yeasar-Practice/projectmanagementtask.git
```

Go to the project directory

```bash
  cd ProjectManagementTask
```

Install dependencies

```bash
  npm install || npm i
```

Start the server

```bash
  npm run dev
```

## Installation

Install my-project with npm

```bash
  npm install || npm i
```

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

**`MONGO_URL `** //it wil be the mongodb database local or cloud server link.

**`PORT `** //it wil be the server port.

**`JWT_CODE `** // it will be the JSON WEB TOKEN'S security code

**`HOST_EMAIL `** // it will be a valid email for nodeMailer host mail. to use this you should have enable low security of you gmail.

**`HOST_PASSWORD `** // it will be a valid email password for nodeMailer host mail. to use this you should have enable low security of you gmail.

**`SENDER_EMAIL`** //it will be a valid email.

**`DATA_URL `** //it will be the server side url

**`TOKEN_EXPIRE `** //It will be a token expire date in days (5 input that's mean 5 days)

**`LOOGGED_IN_USER_SESSON `**/It will be a cookies expire date in days (5 input that's mean 5 days)

## Documentation

- [Database-Design-Doc](https://drive.google.com/file/d/1FHvWi3OSL2kLBE6LxvAVosZzyRUR5mYh/view?usp=sharing)
- [API-OVERVIEW-Doc](https://drive.google.com/file/d/1DK106Zf8km6Oq6ORmouQ_r-bcfABTlNm/view?usp=sharing)
- [POSTMAN-API-FULL-DOC](https://www.postman.com/red-trinity-151066/workspace/assesment-management-task)
- [APU-FULL-DOC](https://drive.google.com/file/d/1BSsUpuBZ1F3K7mD8y4mIYJW0OwZyumvJ/view?usp=sharing)

## Support

For support, sadmanishopnil@gmail.com .
