import convertToRegex from "../../../utils/makeRegex"
import  {
    Request
} from  "express"

type inputAndOutput = {
    $and: Object[]
}
const queryWithFilterAndSearch =  (
    struct:inputAndOutput, 
    searchBy: string,
    showFilter: string,
    req:Request ): void => {
    //convert search by into regex first 
    const searchRegex:Object =  convertToRegex (searchBy)
    //do the searching part 
    searchBy && ( 
        struct.$and.push (
            {
                $or : [
                    { 
                        "title": searchRegex //search by assessment title 
                    },
                    {
                        "assessmentId": searchRegex //search by assessment id
                    },
                ]
            }
        ) 
    )

    //do the show filter part own part
    showFilter && (
        showFilter == "own" 
        &&
        struct.$and.push (
            {
                "mentor": req.user._id
            }
        )
    )
}

const sortingOfAssessmentHandler = (sortBy:string, mainSortingStructure:any): void => {
    sortBy == "a-z" //if sort by ascending order
    &&
    (
        mainSortingStructure["title"] = 1
    )

    sortBy == "z-a" //if sort by descending order
    &&
    (
        mainSortingStructure["title"] = -1
    )

    sortBy == "creationDate" //if sort by descending order
    &&
    (
        mainSortingStructure["createdAt"] = -1
    )

}

export {
    queryWithFilterAndSearch,
    sortingOfAssessmentHandler
}