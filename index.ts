import express, {Application, Request, Response} from "express"
import mongoose from "mongoose"
import dotenv from "dotenv"

const app:Application = express();

const cookieParser = require('cookie-parser')
const cors = require ("cors");
dotenv.config ();


//all route api 
import StudentRoute from "./src/api/Student/route"
import MentorRoute from "./src/api/Mentor/route"
import UserRoute from "./src/api/User/route"
import AssessmentRoute from "./src/api/Assesment/route"
import ResponseRoute from "./src/api/Response/route"


//env file
const url:string = process.env.PORT || "8080"
const mongoUrl:string = process.env.MONGO_URL || "mongodb://localhost:27017/assessmentManagement"

//parser and others middleware part
app.use (express.json({limit: "250mb"}))
app.use (express.urlencoded({extended: true, limit: "250mb"}))
app.use (express.static("public"))
app.use(cookieParser())
app.use (cors())

mongoose.connect (mongoUrl)
.then (() => console.log(`Server is connected to the database`))
.catch (err => console.log(err))



//create a server instance
app.listen (url, () => console.log(`Server is running on ${url}`))



//all rest api
app.use ("/student", StudentRoute)
app.use ("/mentor", MentorRoute)
app.use ("/user", UserRoute)
app.use ("/assessment", AssessmentRoute)
app.use ("/assessment/response", ResponseRoute)


//base route
app.get ("/", (req:Request, res:Response) => {
    res.send (`I am from root`)
})

//page not found route
app.get ("*", (req, res) => {
    res.send (`404 Page not found`)
})
