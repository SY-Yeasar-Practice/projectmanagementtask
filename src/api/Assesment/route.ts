import express,{
    Router
}  from "express"

const route:Router = express.Router();
import {
    createNewAssessmentHandler,
    showAllAssessmentHandler,
    updateAssessmentHandler,
    deleteAssessmentHandler,
    canSeeOnlyOwnAssessment,
    assignStudentForAssessment
} from "./controller"

//middleware 
import auth from "../../../middleware/auth"
import permission from "../../../middleware/authorization"


//get api 
route.get ("/show/own", auth, permission ("student"), canSeeOnlyOwnAssessment)

//post api 
route.post ("/create", auth, permission ("mentor", "admin"), createNewAssessmentHandler)
route.post ("/show/all", auth, permission ("mentor", "admin"), showAllAssessmentHandler)




//put api
route.put ("/update/:slug", auth, permission ("mentor", "admin"), updateAssessmentHandler)
route.put ("/delete/:slug", auth, permission ("mentor", "admin"), deleteAssessmentHandler)
route.put ("/assign/student", auth, assignStudentForAssessment)

export default route