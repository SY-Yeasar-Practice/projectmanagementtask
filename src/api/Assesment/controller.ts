import e, {
    Request,
    Response
} from "express"

//models
import User from "../../model/user"
import Mentor from "../../model/mentor"
import Student from "../../model/student"
import Assessment from "../../model/assesment"


//utils file 
import slugGenerator from "../../../utils/slugGenerator"
import idGenerator from "../../../utils/idGenerator"
import paginationHandler from "../../../utils/paginationHandler"
import {
    queryWithFilterAndSearch,
    sortingOfAssessmentHandler
} from "../../../utils/controller/Assessment/query"
import {
    uploadAnyFile,
    uploadProfilePictureDefault
} from "../../../utils/fileOrPictureUploadHandler"

//interfaces
import AssessmentModelInterface from "../../../src/interfaces/Assesment/model"
import MentorModel from "../../../src/interfaces/Mentor/model"
import paginationReturnInterface from "../../interfaces/utils/paginationReturn"
import {
    createBodyInput,
    showAllAssessmentBodyInputForAll,
    AssignStudentForAssessmentBodyInput
} from "../../../src/interfaces/Assesment/bodyInput"
import MentorProfileInterface from "../../../src/interfaces/Mentor/model"
import {
    UpdateReturn
} from "../../interfaces/utils/mongoDB"


//create a new assessment  
const createNewAssessmentHandler: (req:Request, res:Response) => Promise <void> = async (req, res) =>  {
    try {
        const {
            title,
            description,
            deadline,
            fileBase64
        }:createBodyInput = req.body; //get all data from body

        //first create a new user interface 
        const assessmentId = idGenerator ("ASS") //generate a Assessment User id
        const assessmentSlug = slugGenerator (title) //generate a slug by using Assessment id, gender and department
        let assessmentFileURL:string = "";
        //profile picture upload part for Assessment
        if (fileBase64) { //if Assessment provide base64 then it will happen
            const {
                fileAddStatus,
                fileUrl
            } = await uploadAnyFile (fileBase64, assessmentId, "pdf") //it will upload the file as a pdf format
            fileAddStatus && (assessmentFileURL = fileUrl)
        }
        if (true) {
            const createNewAssessment = new Assessment ({ //create the instance of a Assessment
                slug:assessmentSlug,
                title,
                description,
                deadline,
                file: assessmentFileURL,
                mentor: req.user._id,
                assessmentId
            })
            const saveAssessment:AssessmentModelInterface = await createNewAssessment.save() //save the user 
            if (Object.keys(saveAssessment).length != 0) { //if user create successfully then it will happen
                res.json ({
                    message: "New Assessment has been created successfully",
                    status: 201
                })
            }else {
                res.json ({
                    message: "Assessment Creation failed",
                    status: 406,
                })
            }   
        }
    }catch (err) {
        console.log(err)
        res.json ({
            message: err,
            status: 406
        })
    }
}

//can see all assessment 
const showAllAssessmentHandler: (req:Request, res:Response) => Promise <void> = async (req, res) => {
    try {
        const {
            searchBy,
            showFilter,
            sortBy,
            showData,
            pageNo
        }:showAllAssessmentBodyInputForAll = req.body //get the data from body 
        const getAll = await Assessment.find ()
        const query = {
            $and: [
                {
                    isDelete: false
                }
            ]
        } //main query structure 
        const sorting = {} //main sorting query structure 
        const {
            dataLimit,
            skipData,
            totalPage
        }:paginationReturnInterface =  paginationHandler (+showData, getAll, +pageNo)

        queryWithFilterAndSearch (query, searchBy, showFilter, req) //return the query structure 
        sortingOfAssessmentHandler (sortBy, sorting) //return the sorting structure 
        const getALlAssessment:null | AssessmentModelInterface[] = await Assessment.find (query).sort (sorting).skip(skipData).limit(dataLimit).populate (
            [
                {
                    path: "students",
                    select: `
                        fatherName
                        motherName
                    `,
                    populate: {
                        path: "user",
                        select: `
                            firstName
                            lastName
                            sex
                            userType
                            userId
                            slug
                            dateOfBirth
                            contact
                            profileImage
                        `
                    }
                },
                {
                    path: "mentor",
                    select: `
                        firstName
                        lastName
                        sex
                        userType
                        userId
                        slug
                        dateOfBirth
                        contact
                        profileImage
                    `
                }
            ]
        )
        if (getALlAssessment.length != 0) { //if assessment found 
            res.json ({
                message: `${getALlAssessment.length} assessment found`,
                status: 202,
                data: getALlAssessment,
                totalPage
            })
        }else {
            res.json ({
                message: "No assessment found",
                status: 404,
                data: null,
                totalPage: null
            })
        }
    }catch (err) {
        console.log(err)
        res.json ({
            message: err,
            status: 406,
            data: null,
            totalPage: null
        })
    }
}

// update  assessment by slug

const updateAssessmentHandler: (req:Request, res:Response) => Promise <void> = async (req, res) =>  {
    try {
        const updateAssessment:UpdateReturn = await Assessment.updateOne (
            {
                slug: req.params.slug,
                isDelete: false
            },
            req.body,
            {
                multi: true
            }
        ); 
        if (updateAssessment.modifiedCount != 0) {
            res.json ({
                message: "Update successfully",
                status: 202
            })
        }else {
            res.json ({
                message: "Update failed",
                status: 406
            })
        }
    }catch (err) {
        console.log(err)
        res.json ({
            message: err,
            status: 406
        })
    }
}

//delete assessment handler by slug
const deleteAssessmentHandler: (req:Request, res:Response) => Promise <void> = async (req, res) =>  {
    try {
        const updateAssessment:UpdateReturn = await Assessment.updateOne (
            {
                slug: req.params.slug,
                isDelete: false
            },
            {
                "isDelete": true
            },
            {
                multi: true
            }
        ); 
        if (updateAssessment.modifiedCount != 0) {
            res.json ({
                message: "Delete successfully",
                status: 202
            })
        }else {
            res.json ({
                message: "Delete failed",
                status: 406
            })
        }
    }catch (err) {
        console.log(err)
        res.json ({
            message: err,
            status: 406
        })
    }
}

//can see only own assessment (student role)
const canSeeOnlyOwnAssessment: (req:Request, res:Response) => Promise <void> = async (req, res) => {
    try {
        const studentProfile = await Student.findOne ({user:req.user._id})    
        const findOwnAssessment:AssessmentModelInterface[]  = await Assessment.find ({
            isDelete: false,
            students: {
                $in: studentProfile?._id
            }
        }).populate ({
            path: "mentor",
            select: `
                    lastName
                    firstName
                    email
                    sex
                    contact
                    profileImage
                    userId
                    userType
                `
        }).select (
            `
                mentor
                title
                description
                assessmentId
                slug
                file
                deadline
            `
        )
        if (findOwnAssessment.length != 0) { //if assessment found
            res.json ({
                message: `${findOwnAssessment.length} assessment found`,
                status: 202,
                data: findOwnAssessment
            })
        }else {
            res.json ({
                message: 'No assessment found',
                status: 404,
                data: null
            })
        }
    }catch (err) {
        console.log(err)
        res.json ({
            message: err,
            data: null,
            status: 406
        })
    }
}

//assign multiple student for an assessment (admin and mentor role)
const assignStudentForAssessment: (req:Request, res:Response) => Promise <void> = async (req, res) => {
    try{
        const {
            studentsId,
            assessmentsId
        }: AssignStudentForAssessmentBodyInput = req.body //get the data from body

        const operation = studentsId.map (student => { 
            return {
                updateOne: {
                    filter: {
                        _id:assessmentsId,
                        isDelete: false
                    },
                    update: {
                        $set: {
                            students: student
                        }
                    }
                }
            }
        })
        const insertStudents = await Assessment.bulkWrite (operation);
        if (insertStudents.nModified) { //if assignment done 
                //update each student profile
                const operation = studentsId.map (student => { 
                return {
                    updateOne: {
                        filter: {
                            user :req.user._id
                        },
                        update: {
                            $addToSet: {
                                assessments: assessmentsId
                            }
                        }
                    }
                }
            })
            const updateStudentProfile = await Student.bulkWrite (operation);
            if (updateStudentProfile.nModified != 0) {
                res.json ({
                    message: "Assessment has successfully assigned",
                    status: 202
                })
            }else {
                res.json ({
                    message: "Assignment done but student profile update failed",
                    status: 406
                })
            }
        }else {
            res.json ({
                message: "Assignment failed",
                status: 406
            })
        }

    }catch (err) {
        console.log(err)
        res.json ({
            message: err,
            status: 406
        })
    }
}

export {
    createNewAssessmentHandler,
    showAllAssessmentHandler,
    updateAssessmentHandler,
    deleteAssessmentHandler,
    canSeeOnlyOwnAssessment,
    assignStudentForAssessment
}

// interface deleteAssessmentResponse {
//     message: string,
//     status: number,
// }