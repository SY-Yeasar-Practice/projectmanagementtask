import express,{
    Router
}  from "express"

const route:Router = express.Router();
import {
    registerNewMentor,
    deleteMentorBySlugController,
    updateMentorBySlug,
    getAllMentorController,
    getIndividualMentorBySlug
} from "./controller"

//middleware 
import auth from "../../../middleware/auth"
import permission from "../../../middleware/authorization"


//get api
route.get ("/get/:slug",auth, permission ("admin"), getIndividualMentorBySlug)

//post api 
route.post ("/registration", auth, permission ("admin"), registerNewMentor)
route.post ("/get/all", getAllMentorController)



//put api
route.put ("/delete/:slug", auth, permission ("admin"), deleteMentorBySlugController)
route.put ("/update/:slug", auth, permission ("admin", "mentor"), updateMentorBySlug)

export default route;
