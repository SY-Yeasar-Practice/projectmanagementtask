import express,{
    Router
}  from "express"

const route:Router = express.Router();
import {
    createNewResponseHandler,
    showMentorAllResponseOfAssessmentBySlug,
    showIndividualResponseBySlug,
    remarkResponseBySlug
} from "./controller"

//middleware 
import auth from "../../../middleware/auth"
import permission from "../../../middleware/authorization"

//get api 
route.get ("/show/own/response/:slug", auth, permission ("mentor", "admin"), showMentorAllResponseOfAssessmentBySlug)
route.get ("/show/:slug", auth, permission ("mentor", "admin"), showIndividualResponseBySlug)

//post api 
route.post ("/create", auth, permission ("student"), createNewResponseHandler)

//put api 
route.put ("/remark/:slug", auth, permission ("admin", "mentor"), remarkResponseBySlug)

export default route