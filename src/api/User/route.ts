import express,{
    Router
}  from "express"

const route:Router = express.Router();
import {
    userLoginController,
    forgotPasswordPartOneController,
    verifyTheForgotPasswordOTPController,
    resetPasswordController,
    updateExistingPasswordPartOne,
    updateExistingPasswordPartTwo
} from "./controller"

//middleware 
import auth from "../../../middleware/auth"
import permission from "../../../middleware/authorization"

//get api 

//post api 
route.post ("/login", userLoginController)
route.post ("/forgotPassword/sent/otp", forgotPasswordPartOneController)
route.post ("/forgotPassword/verify/otp", verifyTheForgotPasswordOTPController)
route.post ("/forgotPassword/reset/password", resetPasswordController)
route.post ("/update/existing/password/verify", auth, updateExistingPasswordPartOne)


//put api 
route.put ("/update/existing/password/change", auth, updateExistingPasswordPartTwo)

export default route;