import express,{
    Router
}  from "express"

const route:Router = express.Router();
import {
    registerNewStudentController,
    deleteStudentBySlug,
    updateStudentBySlug,
    showAllStudentHandler,
    showIndividualStudentBySlug
} from "./controller"

//middleware 
import auth from "../../../middleware/auth"
import permission from "../../../middleware/authorization"

//get api 
route.get ("/show/:slug", auth, permission ("admin", "mentor") ,  showIndividualStudentBySlug)
route.get ("/show/:slug", auth, permission ("admin", "mentor") ,  showIndividualStudentBySlug)

//post api 
route.post ("/registration", auth, permission ("admin") ,  registerNewStudentController)
route.post ("/show/all", auth, permission ("admin", "mentor") ,  showAllStudentHandler)


//put api 
route.put ("/delete/:slug",  auth, permission ("admin") , deleteStudentBySlug)
route.put ("/update/:slug",  auth, permission ("admin") , updateStudentBySlug)

export default route;

