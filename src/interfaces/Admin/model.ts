import User from "../User/model"
interface model {
    _id: Object,
    salary:string,
    academicDegree: [
        {
            degreeName: string,
            result: string,
            session: string,
            passingYear: string
        }
    ],
    user: User,
    createdAt: Object,
    updatedAt: Object
}

export default model