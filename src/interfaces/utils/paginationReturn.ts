interface paginationReturnInterface {
    dataLimit: number,
    skipData: number,
    totalPage: number
}
export default paginationReturnInterface