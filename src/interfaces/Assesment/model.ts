import ResponseModelInterface from "../Response/model"
import MentorModelInterface from "../User/model"
import StudentModelInterface from "../Student/model"
import UseModelInterface from "../User/login"

interface model {
    _id: Object,
    responses: [ResponseModelInterface],
    isDelete: boolean,
    mentor: MentorModelInterface,
    title: string,
    description: string,
    deadline: Object,
    students: [StudentModelInterface],
    assessmentId:string,
    slug: string,
    file: string,
    createdAt: Object,
    updatedAt: Object
}

export default model