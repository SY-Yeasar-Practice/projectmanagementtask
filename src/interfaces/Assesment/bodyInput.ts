interface createBodyInput {
    title: string,
    description :string,
    deadline: string,
    fileBase64?: string
}

interface showAllAssessmentBodyInputForAll {
    searchBy: string,
    showFilter: string,
    sortBy: string,
    showData: string,
    pageNo: string
}

interface AssignStudentForAssessmentBodyInput {
    studentsId : string [],
    assessmentsId: string
}

export {
    createBodyInput,
    showAllAssessmentBodyInputForAll,
    AssignStudentForAssessmentBodyInput
}