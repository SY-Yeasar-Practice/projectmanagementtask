interface registrationBodyInput {
    firstName: string,
    lastName : string,
    dateOfBirth: string,
    password: string,
    retypePassword: string,
    motherName: string,
    fatherName: string,
    profileImage: {
        base64:string,
        size:number
    },
    sex:string,
    contactNumber: string,
    email: string,
    permanentAddress: string,
    currentAddress: string
}

interface getAllStudentBodyInput {
    searchBy: string,
    dataLimit: number,
    pageNo: number
}

export {
    registrationBodyInput,
    getAllStudentBodyInput
}