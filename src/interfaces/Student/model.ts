import UserModelInterface from "../User/model"
import AssessmentModelInterface from "../Assesment/model"


interface model {
    _id: Object,
    fatherName: string,
    motherName: string,
    assessments: [AssessmentModelInterface],
    user: UserModelInterface,
    createdAt: Object,
    updatedAt: Object
}

export default model