interface createResponseBodyInput {
    file: {
        base64?: string,
        link?: string
    },
    assessment_id: string,
}

interface remarkAndGradingBodyInput {
    grade: string,
    status: string
}

export {
    createResponseBodyInput,
    remarkAndGradingBodyInput
}