import StudentModelInterface from "../Student/model"
import AssessmentModelInterface from "../Assesment/model"

interface model {
    _id: Object,
    responsesBy: StudentModelInterface,
    src: string,
    grade: string,
    submissionDate: Object,
    status: string ,
    isDelete: boolean,
    slug: string,
    responseId: string,
    assessment: AssessmentModelInterface,
    createdAt: Object,
    updatedAt: Object
}

export default model