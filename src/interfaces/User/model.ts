import AdminInterface from  "../Admin/model"
import MentorInterface from "../Mentor/model"
import StudentInterface from "../Student/model"

interface model{
    _id: Object,
    slug: string,
    userId: string,
    password: string,
    userType: string,
    firstName: string,
    lastName: string,
    email: string,
    dateOfBirth: Object,
    sex: string,
    contact: {
        permanentAddress: string,
        currentAddress: string,
        mobileNo: string
    },
    profileImage: string,
    otp: string,
    isDelete: boolean,
    isActive: boolean,
    adminProfile: AdminInterface ,
    studentProfile: StudentInterface,
    mentorProfile: MentorInterface,
    createdAt: Object,
    updatedAt: Object
}

export default model
