import UserModelInterface from "../User/model"

interface model {
    _id: Object,
    salary: string,
    user: UserModelInterface,
    createdAt: Object,
    updatedAt: Object
}

export default model