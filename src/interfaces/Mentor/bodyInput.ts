interface registrationBodyInput {
    firstName:string,
    lastName: string,
    password: string,
    retypePassword: string,
    dateOfBirth: string,
    profileImage: {
        base64: string,
        size: number
    },
    sex: string,
    salary: string,
    contactNumber: string,
    email: string
    permanentAddress: string,
    currentAddress: string
}

// interface responseInterfaceForMentorDelete {
//     message: string,
//     status: number
// }

interface updateMentorInputInterface <U> {
    common: {
        _id: Object,
        slug: string,
        userId: string,
        password: string,
        userType: string,
        firstName: string,
        lastName: string,
        email: string,
        dateOfBirth: Object,
        sex: string,
        contact: {
            permanentAddress: string,
            currentAddress: string,
            mobileNo: string
        },
        profileImage: string,
        otp: string,
        isDelete: boolean,
        isActive: boolean,
        adminProfile: Object ,
        studentProfile: Object,
        mentorProfile: Object,
        createdAt: Object,
        updatedAt: Object
    },
    profile: U
}

interface getAllBodyInterface {
    searchBy: string,
    dataLimit: number,
    pageNo: number
}
export {
    registrationBodyInput,
    updateMentorInputInterface,
    getAllBodyInterface
}


