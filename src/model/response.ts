import mongoose , {Schema} from "mongoose";
import ResponseModelInterface from "../interfaces/Response/model"


const responseSchema = new Schema <ResponseModelInterface> ({
    responsesBy: {
        type: Schema.Types.ObjectId,
        ref: "Student"
    },
    src: {
        type: String,
        required: true
    },
    grade: {
        type: String
    },
    submissionDate: {
        type: Date,
        required: true
    },
    status: {
        type: String,
        enum: ["pending", "selected", "rejected"],
        default: "pending"
    } ,
    isDelete: {
        type: Boolean,
        default: false
    },
    slug: {
        type: String,
        required: true,
        unique: true
    },
    responseId: {
        type: String,
        required: true,
        unique: true
    },
    assessment: {
        type: Schema.Types.ObjectId,
        ref: "Assessment" 
    }
},{
    timestamps: true
})

export default mongoose.model <ResponseModelInterface> ("Response", responseSchema)