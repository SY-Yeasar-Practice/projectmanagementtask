import mongoose , {Schema} from "mongoose";
import UserModel from "../interfaces/User/model"


const userSchema = new Schema <UserModel>({
    slug: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    userId: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        trim: true,
        max: 32,
        min: 8
    },
    userType: {
        type: String,
        required: true,
        enum: ["mentor", "admin", "student"]
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    dateOfBirth: {
        type: Date,
        required: true
    },
    sex: {
        type: String,
        required: true,
        enum: ["male", "female", "others"]
    },
    contact: {
        permanentAddress: {
            type: String,
            required: true
        },
        currentAddress: {
            type: String,
            required: true
        },
        mobileNo: {
            type: String,
            required: true
        }
    },
    profileImage: {
        type: String,
        required: true
    },
    otp: {
        type: String,
        default: ""
    },
    isDelete: {
        type: Boolean,
        default: false
    },
    isActive: {
        type: Boolean,
        default: true
    },
    adminProfile: {
        type: Schema.Types.ObjectId,
        ref: "Admin"
    },
    studentProfile: {
        type: Schema.Types.ObjectId,
        ref: "Student"
    },
    mentorProfile: {
        type: Schema.Types.ObjectId,
        ref: "Mentor"
    }
},{
    timestamps: true
})

export default mongoose.model <UserModel> ("User", userSchema)