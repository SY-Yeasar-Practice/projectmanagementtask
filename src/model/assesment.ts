import mongoose , {Schema} from "mongoose";
import AssessmentModelInterface from "../interfaces/Assesment/model"


const assessmentSchema = new Schema <AssessmentModelInterface> ({
    responses: [
        {
            type: Schema.Types.ObjectId,
            ref: "Response"
        }
    ],
    isDelete: {
        type: Boolean,
        default: false
    },
    mentor: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    deadline: {
        type: Date,
        required: true
    },
    students: [
        {
            type: Schema.Types.ObjectId,
            ref: "Student"
        }
    ],
    assessmentId: {
        type: String,
        required: true,
        unique: true
    },
    slug: {
        type: String,
        required: true,
        unique: true
    },
    file: {
        type:String
    }
},{
    timestamps: true
})

export default mongoose.model <AssessmentModelInterface> ("Assessment", assessmentSchema)