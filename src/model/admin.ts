import mongoose , {Schema} from "mongoose";
import AdminModel from "../interfaces/Admin/model"


const adminSchema = new Schema <AdminModel>({
    salary: {
        type: String,
        required: true,
    },
    academicDegree: [
        {
            degreeName: {
                type: String,
                required: true,
            },
            result: {
                type: String,
                required: true,
            },
            session: {
                type: String,
                required: true,
            },
            passingYear: {
                type: String,
                required: true
            }
        }
    ],
    user: {
        type: Schema.Types.ObjectId,
        ref: "User"
    }
},{
    timestamps: true
})

export default mongoose.model <AdminModel> ("Admin", adminSchema)